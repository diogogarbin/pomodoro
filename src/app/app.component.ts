import { Component } from '@angular/core';
import { Pomodoro, PomodoroType } from './model/pomodoro.model';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  currentPomodoro?: Pomodoro
  allPomodoros: Pomodoro[] = []
  displayTimer = new Date(0)
  countTasks = 0
  types = PomodoroType

  startPomodoro() {
    this.currentPomodoro = new Pomodoro(this.getPomodoroType())
    this.displayTimer = new Date(0)
    setTimeout(() => this.countTime(), 1000)
  }

  finishPomodoro() {
    
  
  }


  private countTime(){
    this.displayTimer = new Date(this.displayTimer.getTime() + 1000)

    if(this.displayTimer.getTime() > this.currentPomodoro!.maxTime) {
      this.currentPomodoro!.finish = new Date()
      this.allPomodoros.push(this.currentPomodoro!)
      this.startPomodoro()
    }else{
      setTimeout(() => this.countTime(), 1000)
    }
  }


  private getPomodoroType(): PomodoroType {
    if (!this.currentPomodoro || this.currentPomodoro.type != PomodoroType.TASK) {
      this.countTasks ++
      return PomodoroType.TASK

    }else if(this.countTasks == 2){
      this.countTasks = 0
      return PomodoroType.LONG_BREAK
    }

    
    return PomodoroType.BREAK
  }
}
